CU Roster report
=============

This is a simple report which displays the user pictures for everyone enrolled in the given course, adapted from the plugin originally developed by Lafayette College.

Once the plugin is installed, you can access the functionality by going to
Reports > CU Roster within the course.

There are three options when viewing the roster:

- "Printable" view, which brings up a version of the report suitable for printing.
- A "Groups" filtering option