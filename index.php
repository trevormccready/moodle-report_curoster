<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Display a roster of students
 *
 * @package   report_curoster
 */

require_once(dirname(__FILE__) . '/../../config.php');
require_once(dirname(__FILE__) . '/locallib.php');

$id     = required_param('id', PARAM_INT);
$mode   = optional_param('mode', ROSTER_MODE_DISPLAY, PARAM_TEXT);
$group  = optional_param('group', 0, PARAM_INT);
$course = $DB->get_record('course', array('id' => $id), '*', MUST_EXIST);

require_login($course);

// Setup page.
$PAGE->set_url('/report/curoster/index.php', array('id' => $id));
if ($mode === ROSTER_MODE_PRINT) {
    $PAGE->set_pagelayout('print');
} else {
    $PAGE->set_pagelayout('report');
}
$returnurl = new moodle_url('/course/view.php', array('id' => $id));

// Check permissions.
$coursecontext = context_course::instance($course->id);
require_capability('report/curoster:view', $coursecontext);

// Get all the users.
$userlist = get_enrolled_users($coursecontext, '', $group, user_picture::fields('u', null, 0, 0, true));

// Get suspended users.
$suspended = get_suspended_userids($coursecontext);
$nonstudentsql = "SELECT u.id
			FROM {user} u
			JOIN {role_assignments} ra ON u.id = ra.userid
			JOIN {context} ctx ON ra.contextid = ctx.id
			JOIN {course} c ON c.id = ctx.instanceid
			WHERE ra.roleid = 3
			AND c.id = $COURSE->id
			GROUP BY u.id";
$records = $DB->get_recordset_sql($nonstudentsql, array());
$nonstudent = array();
foreach ($records as $record) {
	$nonstudent[] = $record->id;
}

$data = array();
foreach ($userlist as $user) {
    if (!in_array($user->id, $suspended) && !in_array($user->id,$nonstudent)) {
        $item = $OUTPUT->user_picture($user, array('size' => 100, 'courseid' => $course->id));
        $item .= html_writer::tag('span', fullname($user));
        $data[] = $item;
    }
}

// Finish setting up page.
$PAGE->set_title($course->shortname .': '. get_string('roster' , 'report_curoster'));
$PAGE->set_heading($course->fullname);

// Display the roster to the user.
echo $OUTPUT->header();
echo $OUTPUT->heading($COURSE->shortname.' Student '.get_string('roster','report_curoster'));
echo html_writer::tag('h4',$COURSE->fullname,array());
echo report_curoster_output_action_buttons($id, $group, $mode, $PAGE->url);
echo html_writer::alist($data, array('class' => 'report-curoster'));
echo $OUTPUT->footer();
